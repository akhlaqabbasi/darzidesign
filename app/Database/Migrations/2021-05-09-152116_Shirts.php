<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Shirts extends Migration
{
	public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => false
            ],
            'short_desc' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => false,
            ],
            'price' => [
                'type' => 'decimal',
                'null' => false,
            ],
			'color' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => false,
            ],
			'fabric' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => false,
            ],
			'occasion' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => false,
            ],
            'is_custome' => [
                'type' => 'bool',
                'constraint' => '100',
                'null' => false,
            ],
            'updated_at' => [
                'type' => 'datetime',
                'null' => true,
            ],
            'created_at datetime default current_timestamp',
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('shirt_tbl');
    }

	public function down()
	{
		$this->forge->dropTable('shirt_tbl');
	}
}
