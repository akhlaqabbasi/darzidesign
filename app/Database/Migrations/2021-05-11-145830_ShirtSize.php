<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class shirtSize extends Migration
{
	public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'collar_size' => [
                'type' => 'decimal',
                'null' => false
            ],
			'sleeve_length' => [
                'type' => 'decimal',
                'null' => false
            ],
			'chest_measure' => [
                'type' => 'decimal',
                'null' => false
            ],
			'stomach_measure' => [
                'type' => 'decimal',
                'null' => false
            ],
			'collar_size' => [
                'type' => 'decimal',
                'null' => false
            ],
			'fitt_required' => [
                'type' => 'decimal',
                'null' => false
            ],
			'back_length' => [
                'type' => 'decimal',
                'null' => false
            ],
			'height' => [
                'type' => 'decimal',
                'null' => false
            ],
            'weight' => [
                'type' => 'decimal',
                'null' => false
            ],
			'collar_style' => [
				'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => false
            ],
			'cuff_style' => [
				'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => false
            ],
			'placket' => [
				'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => false
            ],
			'pocket' => [
				'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => false
            ],
			'remark' => [
				'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => false
            ],
			
			
            'updated_at' => [
                'type' => 'datetime',
                'null' => true,
            ],
            'created_at datetime default current_timestamp',
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('shirt_size_tbl');
    }

	public function down()
	{
		$this->forge->dropTable('shirt_size_tbl');
	}
}
