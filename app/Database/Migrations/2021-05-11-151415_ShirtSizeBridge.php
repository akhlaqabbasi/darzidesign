<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class shirtSizeRelation extends Migration
{
	public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
			'shirt_id' => [
                'type' => 'INT',
              
            ],
			'size_id' => [
                'type' => 'INT',
                
            ],
           
            'updated_at' => [
                'type' => 'datetime',
                'null' => true,
            ],
            'created_at datetime default current_timestamp',
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('shirt_size_relation_tbl');
    }

	public function down()
	{
		$this->forge->dropTable('shirt_size_relation_tbl');
	}
}
