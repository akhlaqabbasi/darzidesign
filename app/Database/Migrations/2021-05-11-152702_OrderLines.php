<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class orderLines extends Migration
{
	public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
			'order_id' => [
                'type' => 'INT',
              
            ],
			'shirt_id' => [
                'type' => 'INT',
              
            ],
			'shirt_name' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => false
            ],
			'price' => [
                'type' => 'decimal',
                
            ],
			'quantity' => [
                'type' => 'INT',
                
            ],
           
           
            'updated_at' => [
                'type' => 'datetime',
                'null' => true,
            ],
            'created_at datetime default current_timestamp',
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('order_line_tbl');
    }

	public function down()
	{
		$this->forge->dropTable('order_line_tbl');
	}
}
