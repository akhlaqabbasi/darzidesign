<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class order extends Migration
{
	public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
			'user_id' => [
                'type' => 'INT',
              
            ],
			'total_price' => [
                'type' => 'decimal',
              
            ],
			'status' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => false
            ],          
           
            'updated_at' => [
                'type' => 'datetime',
                'null' => true,
            ],
            'created_at datetime default current_timestamp',
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('order_tbl');
    }

	public function down()
	{
		$this->forge->dropTable('order_tbl');
	}
}
