<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;

class ShirtSizeModel extends Model
{
    protected $table = 'shirt_size';
    protected $allowedFields = [
        'collar_size',
        'sleeve_length',
        'chest_measure',
        'stomach_measure',
        'fitt_required',
        'back_length',
        'height',
        'sleeve_length',
        'weight',
        'collar_style',
        'cuff_style',
        'placket',
        'pocket',
        'remark',
    ];
    protected $updatedField = 'updated_at';

}