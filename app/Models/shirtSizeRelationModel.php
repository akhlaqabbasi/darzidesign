<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;

class ShirtSizeRelationModel extends Model
{
    protected $table = 'shirt_size_relation';
    protected $allowedFields = [
        'shirt_id',
        'size_id',
    ];
    protected $updatedField = 'updated_at';

}