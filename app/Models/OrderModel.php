<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;

class OrderModel extends Model
{
    protected $table = 'order_table';
    protected $allowedFields = [
        'user_id',
        'total_price',
        'status',
    ];
    protected $updatedField = 'updated_at';

}