<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;

class OrderLineModel extends Model
{
    protected $table = 'order_line';
    protected $allowedFields = [
        'order_id',
        'shirt_id',
        'shirt_name',
        'price',
        'quantity',
    ];
    protected $updatedField = 'updated_at';

}