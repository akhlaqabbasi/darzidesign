<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;

class CartModel extends Model
{
    protected $table = 'cart_tbl';
    protected $allowedFields = [
        'shirt_id',
        'shirt_name',
        'price',
        'quantity',
    ];
    protected $updatedField = 'updated_at';

}