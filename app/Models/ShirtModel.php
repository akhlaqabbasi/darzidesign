<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;

class ShirtModel extends Model
{
    protected $table = 'shirt';
    protected $allowedFields = [
        'name',
        'short_desc',
        'price',
        'color',
        'fabric',
        'occasion',
    ];
    protected $updatedField = 'updated_at';

}