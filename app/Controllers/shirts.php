<?php

namespace App\Controllers;

use App\Models\ShirtModel;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;

class shirts extends BaseController
{
    /**
     * Get all Clients
     * @return Response
     */
    public function index()
    {  
        $model = new ShirtModel();
        return $this->getResponse(
            [
                'message' => 'Clients retrieved successfully',
                'shirts' => $model->findAll()
            ]
        );
    }

    /**
     * Create a new Client
     */
    public function store()
    {
        $rules = [
            'name' => 'required',
            'short_desc' => 'required',
            'price' => 'required',
            'color' => 'required',
            'fabric' => 'required|min_length[6]|max_length[50]',
            'occasion' => 'required|max_length[255]'
        ];

        $input = $this->getRequestInput($this->request);

        if (!$this->validateRequest($input, $rules)) {
            return $this
                ->getResponse(
                    $this->validator->getErrors(),
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }


        $model = new ShirtModel();
        $model->save($input);
        $id=$model->getInsertID();


        $client = $model->where('id', $id)->first();

        return $this->getResponse(
            [
                'message' => 'Shirt added successfully',
                'client' => $client
            ]
        );
    }

    /**
     * Get a single client by ID
     */
    public function show($id)
    {
        try {

            $model = new ShirtModel();
            $client = $model->find($id);

            return $this->getResponse(
                [
                    'message' => 'shirt retrieved successfully',
                    'client' => $client
                ]
            );

        } catch (Exception $e) {
            return $this->getResponse(
                [
                    'message' => 'Could not find client for specified ID'
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }
    public function update($id)
    {
        try {

            $model = new ShirtModel();
            $model->find($id);

          $input = $this->getRequestInput($this->request);

          

            $model->update($id, $input);
            $client = $model->find($id);

            return $this->getResponse(
                [
                    'message' => 'Client updated successfully',
                    'client' => $client
                ]
            );

        } catch (Exception $exception) {

            return $this->getResponse(
                [
                    'message' => $exception->getMessage()
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }

    public function destroy($id)
    {
        try {

            $model = new ShirtModel();
            $client = $model->findClientById($id);
            $model->delete($client);

            return $this
                ->getResponse(
                    [
                        'message' => 'Client deleted successfully',
                    ]
                );

        } catch (Exception $exception) {
            return $this->getResponse(
                [
                    'message' => $exception->getMessage()
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }
}