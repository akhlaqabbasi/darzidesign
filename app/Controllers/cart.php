<?php

namespace App\Controllers;

use App\Models\ShirtModel;
use App\Models\CartModel;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;

class cart extends BaseController
{
    /**
     * Get all Clients
     * @return Response
     */
    public function index()
    {  
        $model = new ShirtModel();
        return $this->getResponse(
            [
                'message' => 'Clients retrieved successfully',
                'shirts' => $model->findAll()
            ]
        );
    }

    /**
     * Create a new Client
     */
    public function addToCart()
    {
        $input = $this->getRequestInput($this->request);

        $model = new CartModel();
        $cart = $model->where('user_id', $input['user_id'] )
                         -> where('shirt_id',$input['shirt_id'])->first();
            
        if($cart)
        {
            $cart['quantity']+=$input['qty'];
            $model->update($shirt_id, $cart);
            return $this->getResponse(
                [
                    'message' => 'cart updated successfully',
                    
                ]
            );
        }
        else
        {
         
            $shirt=new ShirtModel();
            $shirtincart=$shirt->where('id',$input['shirt_id'])->first();
          
           //echo $shirt->getlastquery();
           
            $model = new CartModel();
             
            $modeldata['shirt_id']=$input['shirt_id'];
            $modeldata['quantity']=$input['user_id'];
            $modeldata['shirt_name']=$shirtincart['name'];
            $modeldata['user_id']=$input['user_id'];
            $modeldata['price]']=$shirtincart['price'];
           
            $model->save($modeldata);
            return $this->getResponse(
                [
                    'message' => 'add to cart successfully',
                    
                ]
            );
        }
       
        


      
    }

    /**
     * Get a single client by ID
     */
    public function show($id)
    {
        try {

            $model = new ShirtModel();
            $client = $model->find($id);

            return $this->getResponse(
                [
                    'message' => 'shirt retrieved successfully',
                    'client' => $client
                ]
            );

        } catch (Exception $e) {
            return $this->getResponse(
                [
                    'message' => 'Could not find client for specified ID'
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }
    public function update($id)
    {
        try {

            $model = new ShirtModel();
            $model->find($id);

          $input = $this->getRequestInput($this->request);

          

            $model->update($id, $input);
            $client = $model->find($id);

            return $this->getResponse(
                [
                    'message' => 'Client updated successfully',
                    'client' => $client
                ]
            );

        } catch (Exception $exception) {

            return $this->getResponse(
                [
                    'message' => $exception->getMessage()
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }

    public function destroy($id)
    {
        try {

            $model = new ShirtModel();
            $client = $model->findClientById($id);
            $model->delete($client);

            return $this
                ->getResponse(
                    [
                        'message' => 'Client deleted successfully',
                    ]
                );

        } catch (Exception $exception) {
            return $this->getResponse(
                [
                    'message' => $exception->getMessage()
                ],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
    }
}